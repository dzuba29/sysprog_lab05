#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <time.h>

int main()
{   
    unsigned char temp_hash = 0;
    size_t mem_size = 1000001;
    char pathname[] = "a.c"; 
    struct sembuf init,lock,unlock;

    key_t key = ftok(pathname, 0); //ключ для общей памяти
    if (key < 0)
    {
        printf("Can\'t generate key\n");
        exit(-1);
    } 
     
    int semid = semget(key, 1, 0666 | IPC_CREAT);
    if(semid < 0)
    {
      printf("Can\'t get semid\n");
      exit(-1);
    }
    

    int shmid = shmget(key,mem_size,0666 | IPC_CREAT | IPC_EXCL); //дескриптор 
    if (shmid < 0)
    {

        if (errno != EEXIST)
        {
            printf("Can\'t create shared memory\n");
            exit(-1);
        }
        else
        {

            if ((shmid = shmget(key,mem_size, 0)) < 0)
            {
                printf("Can\'t find shared memory\n");
                exit(-1);
            }
        }
    }

    void *shared_mem = shmat(shmid, NULL, 0); //ptr на общую память
    if (shared_mem  == (int *)(-1))
    {
        printf("Can't attach shared memory\n");
        exit(-1);
    }
    clock_t time;
    int *array = (int *)shared_mem;
    unsigned char *hash = array +250000; //сдвиг до ласт элемента - хеша 

    int *temp = malloc(mem_size);

    init.sem_op =  0;
    init.sem_flg = 0;
    init.sem_num = 0;

    lock.sem_op =  1;
    lock.sem_flg = 0;
    lock.sem_num = 0;

    unlock.sem_op =  -1;
    unlock.sem_flg = 0;
    unlock.sem_num = 0;

    for(size_t j=0;j<100;++j)
    {
        temp_hash = 0;
        for(size_t i=0;i<250000-1;++i)
        {
            temp[i]=rand()%10;
            temp_hash+=temp[i];

        }

        if(semop(semid, &init, 1) < 0)
        {
            printf("Can\'t init sem\n");
            exit(-1);
        }

        printf("LOCK\n");
        time = clock();
        if(semop(semid, &lock, 1) < 0)
        {
            printf("Can\'t lock sem\n");
            exit(-1);
        }
        time = clock() - time;
        printf("%f\n",(double)time/CLOCKS_PER_SEC);

        memcpy(array, temp,mem_size);
        *hash = temp_hash;
        printf("HASH: %d \n",temp_hash);

        printf("UNLOCK\n");
        if(semop(semid, &unlock, 1) < 0)
        {
            printf("Can\'t unlock sem\n");
            exit(-1);
        }        

        usleep(100000); // милисекунды
    }

    if (shmdt(array) < 0)
    {
        printf("Can't detach shared memory\n");
        exit(-1);
    }
    return 0;
}

